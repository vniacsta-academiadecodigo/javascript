$(document).ready(function () {
    function successCallback(response) {
        let table = $("#table");

        response.forEach((i) => {
            $(table).append(`
                <tr>
                <td>${i.firstName}</td>
                <td>${i.lastName}</td>
                <td>${i.email}</td>
                <td>${i.phone}</td>
                </tr>`);
        });
    }

    function errorCallback(request, status, error) {
        console.log(`Request: ${request}\nStatus: ${status}\nError: ${error}`);
    }

    $.ajax({
        url: "http://localhost:8080/javabank5/api/customer",
        async: true,
        success: successCallback,
        error: errorCallback,
    });

    let dataPost = {
        firstName: "luciano",
        lastName: "costa",
        email: "vania@g.co",
        phone: "81848457555",
    };

    $.post("http://localhost:8080/javabank5/api/customer/", dataPost);
});
