window.onload = function () {
    let url = "http://localhost:8080/javabank5/api/customer";

    axios.get(url).then(getInfo);

    axios
        .post("http://localhost:8080/javabank5/api/customer/", {
            firstName: "blabka",
            lastName: "Flintstone",
            email: "vania@c.com",
            phone: "4362526283",
        })
        .then(function (response) {
            console.log(response);
        });
};

function getInfo(response) {
    let table = document.getElementById("table");
    console.log(response);
    let info = response.data;

    info.forEach((i) => {
        table.insertRow().innerHTML = `
            <td>${i.firstName}</td>
            <td>${i.lastName}</td>
            <td>${i.email}</td>
            <td>${i.phone}</td>`;
    });
}
