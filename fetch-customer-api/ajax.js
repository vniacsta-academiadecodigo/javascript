window.onload = function () {
    let url = "http://localhost:8080/javabank5/api/customer/";

    let xhr;

    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            // fillTable(xhr.responseText);
        }
    };

    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send();
};

// function fillTable(response) {
//     let table = document.getElementById("table");

//     let info = response.data;

//     info.forEach((i) => {
//         table.insertRow().innerHTML = `
//             <td>${i.firstName}</td>
//             <td>${i.lastName}</td>
//             <td>${i.email}</td>
//             <td>${i.phone}</td>`;
//     });
// }
