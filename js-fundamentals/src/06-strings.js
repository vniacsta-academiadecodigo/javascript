/**
 * Reduce duplicate characters to a desired minimum
 */
exports.reduceString = function (str, amount) {
    let regex = new RegExp("(.)(?=\\1{" + amount + "})", "g");
    return str.replace(regex, "");
};

/**
 * Wrap lines at a given number of columns without breaking words
 */
exports.wordWrap = function (str, cols) {};

/**
 * Reverse a String
 */
exports.reverseString = function (str) {
    return str.split("").reverse().join("");
};

/**
 * Check if String is a palindrome
 */
exports.palindrome = function (str) {
    str.replace(/\W/g, "").toLowerCase();
    return str === str.split("").reverse().join("");
};
