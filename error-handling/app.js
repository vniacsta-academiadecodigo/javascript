fetchData(processResults);

function fetchData(cb) {
    $.ajax({
        url: "https://api.ratesapi.io/api/latest",
        type: "GET",
        dataType: "json",
        success: function (results) {
            cb(null, results);
        },
        error: function (request, statusText, httpError) {
            cb(httpError || statusText);
        },
    });
}

function processResults(error, data) {
    if (error) {
        document.getElementById("result").innerHTML = "Error fetching rates";
    }

    document
        .getElementById("submit")
        .addEventListener("click", function (event) {
            event.preventDefault();

            let amountEUR = document.getElementById("inputAmount").value;
            let amountUSD = data.rates.USD;

            document.getElementById(
                "result"
            ).innerHTML = `${amountEUR} EUR = ${Math.round(
                amountEUR * amountUSD
            )} USD`;
        });
}
