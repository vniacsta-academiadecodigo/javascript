document.getElementById("submit").addEventListener("click", function (event) {
    event.preventDefault();

    let user = document.getElementById("inputUser");
    let url = `https://api.github.com/search/users?q=${user.value}&sort=joined&order=asc`;

    // to clean the input
    user.value = "";

    // call the api with axios and then call the rest of the functions
    axios.get(url).then(getUser).catch(onError);
});

function getUser(response) {
    console.log(response);
    let results = response.data.items;
    results.forEach(
        (i) =>
            (document.getElementById("result").innerHTML += `
        <p><span class='github-id'>${i.id}</span><span><img src='${i.avatar_url}' alt='avatar' /></span><span>${i.login}</span></p>`)
    );
}

function onError() {
    document.getElementById("result").innerHTML +=
        "<p style='color: darkred'>TypeError: Failed to fetch</p>";
}
